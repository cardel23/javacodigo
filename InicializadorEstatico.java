import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

//Primero declarar variables 
    static Scanner scan = new Scanner(System.in);
    static int B = 0, H = 0;
    static int getB(Scanner scan){         
            return scan.nextInt();
        }
    static int getH(Scanner scan){
            return scan.nextInt();
        }

    static boolean checkFlag(){
            if(B<=0 || H<=0){
                System.out.println("java.lang.Exception: Breadth and height must be positive");
                return false;
            } else return true;
        }
    static boolean flag = false;
//Inicializador antes de main
    static{

        B = getB(scan);
        H = getH(scan);
        flag = checkFlag();
    }

public static void main(String[] args){
		if(flag){
			int area=B*H;
			System.out.print(area);
		}
		
	}//end of main

}//end of class
